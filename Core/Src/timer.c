/*
 * timer.c
 *
 *  Created on: Dec 6, 2023
 *      Author: johannes
 */

#include "timer.h"

void timer_init(Timer_t *_timer)
{}

void timer_sequence_start(Timer_t *_timer, uint64_t millis)
{
	_timer->end = millis;
	_timer->tick = 0;
	_timer->running = true;
}

void timer_sequence_stop(Timer_t *_timer)
{
	_timer->running = false;
}

void timer_sequence_set_timeout_handler(Timer_t *_timer, sequence_timer_handler_t timeout_handler)
{
	_timer->sequence_timeout_handler = timeout_handler;
}

void timer_sequence_step_callback(Timer_t *_timer)
{
	if(_timer->running)
	{
		_timer->tick++;

		if(_timer->end <= _timer->tick) {
			_timer->running = false;
			_timer->sequence_timeout_handler(_timer->engine);
		}
	}
}





