/*
 * microcode_manager.c
 *
 *  Created on: Dec 10, 2023
 *      Author: johannes
 */

#define OD_DEFINITION
#include "microcode_manager.h"
#include "gpioasm.h"

void MicrocodeManager_init(MicrocodeManager_t *_microcode_manager, TIM_HandleTypeDef *timer_handle, gpioasm_engine_init_t *_gpioasm_init, uint32_t _engine_count)
{
#ifdef DEBUG_MICROCODE
	LOG_DEBUG("Microcode manager: Init function called\r\n");
#endif

	// memory allocation
	_microcode_manager->size = _engine_count;
//	_microcode_manager->engines = (gpioasm_engine_t*)malloc(_microcode_manager->size * sizeof(gpioasm_engine_t));
//	_microcode_manager->timers = (Timer_t*)malloc(_microcode_manager->size * sizeof(Timer_t));

#ifdef DEBUG_MICROCODE
	if(_microcode_manager->engines == NULL)
		LOG_ERROR("Microcode manager: Failed allocating memory for gpioasm engines\r\n");
	if(_microcode_manager->timers == NULL)
		LOG_ERROR("Microcode manager: Failed allocating memory for gpioasm timers\r\n");
#endif

	  // start timer
	  HAL_TIM_Base_Start_IT(timer_handle);

	// init timers
	for (uint32_t i = 0; i < _microcode_manager->size; ++i) {
		_microcode_manager->timers[i].htim = timer_handle;
		_microcode_manager->timers[i].engine = _microcode_manager->engines + i;	// <=> *_ga_engines[i]
		_microcode_manager->timers[i].sequence_timeout_handler = gpioasm_handle_timer_timeout;
	}

	// init gpioasm
	for (uint32_t i = 0; i < _microcode_manager->size; ++i) {
		  gpioasm_init(_microcode_manager->engines + i, _gpioasm_init);
	}
}

void MicrocodeManager_handleInputChange(MicrocodeManager_t *microcode_manager, uint32_t index, uint8_t state){
	for(int i = 0; i < MICROCODE_ENGINE_COUNT; i++){
		gpioasm_handle_digital_input_update(microcode_manager->engines + i, index, state);
	}
}

void MicrocodeManager_start(MicrocodeManager_t *_microcode_manager, uint32_t index)
{
	if(index < MICROCODE_ENGINE_COUNT) {
		// start microcode execution
		gpioasm_start(_microcode_manager->engines + index);
		return;
	}
#ifdef DEBUG_MICROCODE
	else{
		LOG_ERROR("Microcode manager: Failed loading microcode from OD\r\n");
	}
#endif
}

void MicrocodeManager_stop(MicrocodeManager_t *_microcode_manager, uint32_t index)
{
	// start microcode execution
	gpioasm_stop(_microcode_manager->engines + index);
}

void MicrocodeManager_restart(MicrocodeManager_t *_microcode_manager, uint32_t index)
{
	// start microcode execution
	MicrocodeManager_stop(_microcode_manager, index);

	gpioasm_reset(_microcode_manager->engines + index);

	MicrocodeManager_start(_microcode_manager, index);
}







