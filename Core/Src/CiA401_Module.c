/*
 * CiA401.c
 *
 *  Created on: Nov 30, 2023
 *      Author: Johannes Hofmann
 */

#include "CiA401_Module.h"
#include "gpioasm.h"

#ifdef DIGITAL_INPUT_ENABLE
void CiA401_ProcessDigitalInput(MicrocodeManager_t *microcode_manager)
{
	uint8_t current_input;
	DigitalInput_t *di;

	for (uint32_t input_index = 0; input_index < pin_manager.size_digital_input; ++input_index)
	{
		di = pin_manager.digital_input + input_index;

		// read GPIO pin
		current_input = HAL_GPIO_ReadPin(di->pin.GPIO_Port, di->pin.GPIO_Pin);

		// TODO implement filter constant used in de-bounce filter

		// check if polarity is negative
		current_input ^= *di->p_input_polarity;

		uint8_t last_known_state = *(di->p_last_known_state);

		// update input entry in OD
		*(di->p_last_known_state) = current_input;

		// check if input should be ignored
		if(*(di->p_input_it_enable) == false){
			continue;
		}

		// check if input has changed at all
		if(current_input == last_known_state){
			continue;
		}

		// check for changes
		//  input change occurred

		// generate interrupt mask (see doc for more info)
		bool should_report_entry = false;

		if(*(di->p_trigger_direction_both)){
			// we are here since the current values is not equal to the last known value
			// hence, send an update
			should_report_entry = true;
		}else if(*(di->p_trigger_direction_rising)){
			// only report if new value is HIGH (risen)
			// could we simplify this? yes.
			// leaving it as is for readability
			if(current_input){
				should_report_entry = true;
			}
		}else if(*(di->p_trigger_direction_falling)){
			// only report if new value is LOW (fallen)
			if(!current_input){
				should_report_entry = true;
			}
		}

		if(should_report_entry) {
			// send TPDO
			CO_TPDOsendRequest(&canOpenNodeSTM32.canOpenStack->TPDO[input_index]);

			MicrocodeManager_handleInputChange(microcode_manager, input_index, current_input);
		}
	}
}
#endif

#ifdef DIGITAL_OUTPUT_ENABLE
void CiA401_ProcessDigitalOutput(void)
{
	// TODO Device Error Handeling
	bool_t device_error = false;

	uint8_t output;
	DigitalOutput_t *dout;

	for (uint32_t i = 0; i < pin_manager.size_digital_output; ++i)
	{
		dout = &pin_manager.digital_output[i];

		if(!device_error) {
			output = *dout->p_output_entry;

		}else{
			if(*dout->p_output_error_mode == 0) {
				/* do nothing */
			}else{
				output = *dout->p_output_error_value;
			}
		}

		// change polarity
		output ^= *dout->p_output_polarity;

		// TODO filter

		// TODO check for changes
		if(1) {
			// write GPIO pin
			HAL_GPIO_WritePin(dout->pin.GPIO_Port, dout->pin.GPIO_Pin, output == 1 ? GPIO_PIN_SET : GPIO_PIN_RESET);
		}
	}
}
#endif

#ifdef ANALOG_INPUT_ENABLE
void CiA401_ProcessAnalogInput(void)
{
	// TODO implement CiA401_ProcessAnalogInput
}
#endif


#ifdef ANALOG_OUTPUT_ENABLE
void CiA401_ProcessAnalogOutput(void)
{
	// TODO Device Error Handeling
	bool_t device_error = false;

	// TODO support other value types
	uint8_t value8;
	AnalogOutput_t *ao;

	for (uint32_t i = 0; i < pin_manager.size_analog_output; ++i)
	{
		ao = &pin_manager.analog_output[i];

		if(!device_error) {
			value8 = *ao->p_value_8;

		}else{

			if(*ao->p_error_mode == 0) {
				/* do nothing */
			}else{
				value8 = (uint8_t)*ao->p_error_value_32;
			}
		}

		// check for changes
		if(ao->old_value_8 != value8) {
			__HAL_TIM_SET_COMPARE(ao->htim, ao->channel, (uint32_t)value8);	// invert output due to open drain configuration

			ao->old_value_8 = value8;
		}
	}
}
#endif
