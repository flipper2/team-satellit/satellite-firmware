/*
 * timer.h
 *
 *  Created on: Dec 6, 2023
 *      Author: johannes
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include "stm32f4xx.h"

#include "gpioasm.h"

#define false 0
#define true 1

//typedef void (*debounce_timer_timeout_handler_t)(uint32_t timer_index);

//struct Timer_t;
typedef void (*sequence_timer_handler_t)(gpioasm_engine_t *engine);

typedef struct {

	TIM_HandleTypeDef *htim;
	gpioasm_engine_t *engine;

	uint64_t end;
	uint64_t tick;
	bool running;

	sequence_timer_handler_t sequence_timeout_handler;

} Timer_t;

void timer_init(Timer_t *_timer);
void timer_sequence_start(Timer_t *_timer, uint64_t millis);
void timer_sequence_stop(Timer_t *_timer);
void timer_sequence_set_timeout_handler(Timer_t *_timer, sequence_timer_handler_t timeout_handler);

void timer_sequence_step_callback(Timer_t *_timer);


#endif /* TIMER_H_ */
